package main

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

type ItemModerationChaincode struct{}

type ItemModeration struct {
	ItemType      string `json:"itemType"`
	ItemId        string `json:"itemId"`
	ExtExaminerId uint64 `json:"extExaminerId"`
	Status        bool   `json:"status"`
	CreateDate    int64  `json:"createDate"`
	LastUpdate    int64  `json:"lastUpdate"`
	DocType       string `json:"docType"`
}

const (
	CreateItemModeration          string = "createItemModeration"
	UpdateItemModeration          string = "updateItemModeration"
	GetModerationByItem           string = "getModerationByItem"
	GetModerationsByExtExaminerId string = "getModerationsByExtExaminerId"
	GetModerationHistoryByItem    string = "getModerationHistoryByItem"
	GetAllModerations             string = "getAllModerations"
)

func (itemCcPtr *ItemModerationChaincode) createItemModeration(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("Executing createItemModeration() args=" + strings.Join(args, ", "))

	numArgs := 4
	if len(args) != numArgs {
		return shim.Error("Invalid num of args. Expected " + strconv.Itoa(numArgs))
	}

	var itemType string
	var itemId string
	var extExaminerId uint64
	var status bool

	itemType = args[0]
	itemId = args[1]
	extExaminerId, _ = strconv.ParseUint(args[2], Base, BitSize)
	status, _ = strconv.ParseBool(args[3])

	var itemModeration = ItemModeration{
		ItemType:      itemType,
		ItemId:        itemId,
		ExtExaminerId: extExaminerId,
		Status:        status,
		CreateDate:    GetCurrentTimestamp(),
		LastUpdate:    GetCurrentTimestamp(),
		DocType:       ItemModDocType}

	itemModerationAsBytes, err := json.Marshal(itemModeration)
	if err != nil {
		return shim.Error(err.Error())
	}

	key := ItemModKey + itemId
	err = stub.PutState(key, itemModerationAsBytes)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(nil)
}

func (itemCcPtr *ItemModerationChaincode) updateItemModeration(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("Executing updateItemModeration() args=" + strings.Join(args, ", "))

	numArgs := 3
	if len(args) != numArgs {
		return shim.Error("Invalid num of args. Expected " + strconv.Itoa(numArgs))
	}

	var extExaminerId uint64
	var itemId string
	var status bool

	extExaminerId, _ = strconv.ParseUint(args[0], Base, BitSize)
	itemId = args[1]
	status, _ = strconv.ParseBool(args[2])

	key := ItemModKey + itemId

	itemModerationAsBytes, err := stub.GetState(key)
	if err != nil {
		return shim.Error(err.Error())
	} else if itemModerationAsBytes == nil {
		return shim.Error("Moderation item specified doesn't exist")
	}

	updatedItemMod := ItemModeration{}

	err = json.Unmarshal(itemModerationAsBytes, &updatedItemMod)
	if err != nil {
		return shim.Error(err.Error())
	}

	if extExaminerId != updatedItemMod.ExtExaminerId {
		return shim.Error("extExaminerId not authorised to update this item")
	}

	updatedItemMod.Status = status
	updatedItemMod.LastUpdate = GetCurrentTimestamp()

	updatedItemModAsBytes, err := json.Marshal(updatedItemMod)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = stub.PutState(key, updatedItemModAsBytes)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

func (itemCcPtr *ItemModerationChaincode) getModerationByItem(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("Executing getModerationByItem() args=" + strings.Join(args, ", "))

	numArgs := 2
	if len(args) != numArgs {
		return shim.Error("Invalid num of args. Expected " + strconv.Itoa(numArgs))
	}

	var itemType string
	var itemId string

	itemType = args[0]
	itemId = args[1]

	queryString := fmt.Sprintf(`
		{
			"selector": {"docType": "%s", "itemType": "%s", "itemId": "%s"},
			"use_index": ["ddocItem", "indexItem"]
		}`, ItemModDocType, itemType, itemId)

	queryResult, err := GetQueryResultFromQueryString(stub, queryString)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResult)
}

func (itemCcPtr *ItemModerationChaincode) getModerationsByExtExaminerId(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("Executing getModerationsByExtExaminerId() args=" + strings.Join(args, ", "))

	numArgs := 1
	if len(args) != numArgs {
		return shim.Error("Invalid num of args. Expected " + strconv.Itoa(numArgs))
	}

	var extExaminerId uint64
	extExaminerId, _ = strconv.ParseUint(args[0], Base, BitSize)

	queryString := fmt.Sprintf(`
		{
			"selector": {"docType": "%s", "extExaminerId": %d, "lastUpdate": {"$gt": 0}},
			"sort": [{"lastUpdate": "desc"}],
			"use_index": ["ddocExtExaminer", "indexExtExaminer"]
		}`, ItemModDocType, extExaminerId)

	queryResult, err := GetQueryResultFromQueryString(stub, queryString)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResult)
}

func (itemCcPtr *ItemModerationChaincode) getModerationHistoryByItem(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("Executing getModerationHistoryByItem() args=" + strings.Join(args, ", "))

	numArgs := 1
	if len(args) != numArgs {
		return shim.Error("Invalid num of args. Expected " + strconv.Itoa(numArgs))
	}

	//TODO: Figure out way to use item Type as well to get history. Potential bug!
	var itemId string
	itemId = args[0]

	queryResult, err := GetHistoryById(stub, itemId)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResult)
}

func (itemCcPtr *ItemModerationChaincode) getAllModerations(stub shim.ChaincodeStubInterface) pb.Response {
	fmt.Println("Executing getAllModerations()")

	queryString := fmt.Sprintf(`
		{
			"selector": {"docType": "%s", "createDate": {"$gt": 0}},
			"sort": [{"createDate": "desc"}],
			"use_index": ["ddocItemModeration", "indexItemModeration"]
		}`, ItemModDocType)

	queryResult, err := GetQueryResultFromQueryString(stub, queryString)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResult)
}

func (itemModCcPtr *ItemModerationChaincode) Init(stub shim.ChaincodeStubInterface) pb.Response {
	return shim.Success(nil)
}

func (itemModCcPtr *ItemModerationChaincode) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	fn, args := stub.GetFunctionAndParameters()
	fmt.Println("Invoke() is running: " + fn)

	switch fn {
	case CreateItemModeration:
		return itemModCcPtr.createItemModeration(stub, args)

	case UpdateItemModeration:
		return itemModCcPtr.updateItemModeration(stub, args)

	case GetModerationByItem:
		return itemModCcPtr.getModerationByItem(stub, args)

	case GetModerationsByExtExaminerId:
		return itemModCcPtr.getModerationsByExtExaminerId(stub, args)

	case GetModerationHistoryByItem:
		return itemModCcPtr.getModerationHistoryByItem(stub, args)

	case GetAllModerations:
		return itemModCcPtr.getAllModerations(stub)

	default:
		fmt.Println("Invoke() did not find fn: " + fn)
		return shim.Error("Could not find fn: " + fn)
	}
}
