package main

import (
	"fmt"
	"github.com/hyperledger/fabric/core/chaincode/shim"
)

func main() {
	err := shim.Start(new(ItemModerationChaincode))
	if err != nil {
		fmt.Printf("Error starting Item Moderation Chaincode: %s", err)
	}
}
