package main

import (
	"bytes"
	"fmt"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"strconv"
	"strings"
	"time"
)

const CommentKey = "COMMENT:"
const CommentDocType = "comment"

const Base = 10
const BitSize = 64

func GetCurrentTimestamp() int64 {
	return time.Now().UnixNano() / int64(time.Millisecond)
}

func GetHistoryById(stub shim.ChaincodeStubInterface, id string) ([]byte, error) {
	fmt.Println("Executing getHistoryById() id=" + id)

	key := CommentKey + id
	historyQueryIterator, err := stub.GetHistoryForKey(key)
	if err != nil {
		return nil, err
	}

	defer historyQueryIterator.Close()

	var buffer bytes.Buffer
	buffer.WriteString("[")

	arrayMemberWritten := false

	for historyQueryIterator.HasNext() {
		queryResult, err := historyQueryIterator.Next()
		if err != nil {
			return nil, err
		}

		if arrayMemberWritten == true {
			buffer.WriteString(", ")
		}

		buffer.WriteString("{\"txId\":")
		buffer.WriteString("\"")
		buffer.WriteString(queryResult.GetTxId())
		buffer.WriteString("\"")

		var value string
		value = string(queryResult.GetValue())
		value = strings.Replace(value, "\"", "\\\"", -1)

		buffer.WriteString(", \"value\": \"")
		buffer.WriteString(value)
		buffer.WriteString("\"")

		var timestampInMillis string
		timestampInMillis = string(strconv.FormatInt(queryResult.GetTimestamp().GetSeconds(), Base))

		buffer.WriteString(", \"timestamp\": \"")
		buffer.WriteString(timestampInMillis)
		buffer.WriteString("\"")

		buffer.WriteString(", \"isDelete\": \"")
		buffer.WriteString(strconv.FormatBool(queryResult.GetIsDelete()))
		buffer.WriteString("\"}")

		arrayMemberWritten = true
	}

	buffer.WriteString("]")

	fmt.Printf("Finished executing getHistoryById id: %s\n, queryResult: \n%s", id, buffer.String())

	return buffer.Bytes(), nil
}

//TODO: Add this to base_chaincode.go to make global
func GetQueryResultFromQueryString(stub shim.ChaincodeStubInterface, queryString string) ([]byte, error) {
	fmt.Printf("Executing getQueryResultFromQueryString() for queryString: %s\n\n", queryString)

	resultsIterator, err := stub.GetQueryResult(queryString)
	if err != nil {
		return nil, err
	}
	defer resultsIterator.Close()

	var buffer bytes.Buffer
	buffer.WriteString("[")

	arrayMemberWritten := false
	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return nil, err
		}

		if arrayMemberWritten == true {
			buffer.WriteString(",")
		}

		var record string
		record = string(queryResponse.Value)
		record = strings.Replace(record, "\"", "\\\"", -1)

		buffer.WriteString("{\"key\":")
		buffer.WriteString("\"")
		buffer.WriteString(queryResponse.Key)
		buffer.WriteString("\"")

		buffer.WriteString(", \"record\": \"")
		buffer.WriteString(record)
		buffer.WriteString("\"}")
		arrayMemberWritten = true
	}
	buffer.WriteString("]")

	fmt.Printf("Finished executing getQueryResultFromQueryString queryString: %s\n, queryResult: \n%s", queryString, buffer.String())

	return buffer.Bytes(), nil
}
