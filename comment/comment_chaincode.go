package main

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

type CommentChaincode struct{}

type Comment struct {
	Id            string `json:"id"`
	Text          string `json:"text"`
	SenderType    string `json:"senderType"`
	SenderId      uint64 `json:"senderId"`
	RecipientType string `json:"recipientType"`
	RecipientId   uint64 `json:"recipientId"`
	ItemType      string `json:"itemType"`
	ItemId        string `json:"itemId"`
	ExamId        string `json:"examId"`
	CreateDate    int64  `json:"createDate"`
	LastUpdate    int64  `json:"lastUpdate"`
	Deleted       bool   `json:"deleted"`
	DeleteDate    int64  `json:"deleteDate"`
	DocType       string `json:"docType"`
}

func (comment Comment) String() string {
	return fmt.Sprintf(`
		Comment {
			id=%s,
			text=%s,
			senderType=%s,
			senderId=%d,
			recipientType=%s,
			recipientId=%d,
			itemType=%s,
			itemId=%s,
			examId=%s,
			createDate=%d,
			lastUpdate=%d,
			deleted=%v,
			deleteDate=%d,
			docType=%s
		}`,
		comment.Id,
		comment.Text,
		comment.SenderType,
		comment.SenderId,
		comment.RecipientType,
		comment.RecipientId,
		comment.ItemType,
		comment.ItemId,
		comment.ExamId,
		comment.CreateDate,
		comment.LastUpdate,
		comment.Deleted,
		comment.DeleteDate,
		comment.DocType)
}

const (
	CreateComment          string = "createComment"
	GetCommentsByItem      string = "getCommentsByItem"
	GetCommentsByExam      string = "getCommentsByExam"
	GetCommentsBySender    string = "getCommentsBySender"
	GetCommentsByRecipient string = "getCommentsByRecipient"
	DeleteComment          string = "deleteComment"
	GetAllComments         string = "getAllComments"
	GetCommentHistoryById  string = "getCommentHistoryById"
	GetCommentById         string = "getCommentById"
)

//TODO: Create comments for examination approvals

func (commentCcPtr *CommentChaincode) Init(stub shim.ChaincodeStubInterface) pb.Response {
	return shim.Success(nil)
}

func (commentCcPtr *CommentChaincode) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	fn, args := stub.GetFunctionAndParameters()
	fmt.Println("Invoke() has fn: " + fn)

	switch fn {

	case CreateComment:
		return commentCcPtr.createComment(stub, args)

	case GetCommentsByItem:
		return commentCcPtr.getCommentsByItem(stub, args)

	case GetCommentsByExam:
		return commentCcPtr.getCommentsByExam(stub, args)

	case GetCommentsBySender:
		return commentCcPtr.getCommentsBySender(stub, args)

	case GetCommentsByRecipient:
		return commentCcPtr.getCommentsByRecipient(stub, args)

	case DeleteComment:
		return commentCcPtr.deleteComment(stub, args)

	case GetAllComments:
		return commentCcPtr.getAllComments(stub)

	case GetCommentHistoryById:
		return commentCcPtr.getCommentHistoryById(stub, args)

	case GetCommentById:
		return commentCcPtr.getCommentById(stub, args)

	default:
		fmt.Println("Did not find fn: " + fn)
		return shim.Error("Unknown function")
	}
}

func (commentCcPtr *CommentChaincode) getCommentById(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("Executing getCommentById() args=" + strings.Join(args, ", "))

	numArgs := 1
	if len(args) != numArgs {
		return shim.Error("Invalid num of args. Expected " + strconv.Itoa(numArgs))
	}

	var commentId string
	commentId = args[0]

	queryString := fmt.Sprintf(`
		{
			"selector": {"docType": "%s", "id": "%s"},
			"use_index": ["ddocComment", "indexComment"]
		}`, CommentDocType, commentId)

	queryResult, err := GetQueryResultFromQueryString(stub, queryString)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResult)
}

func (commentCcPtr *CommentChaincode) getCommentHistoryById(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("Executing getCommentHistoryById() args=" + strings.Join(args, ", "))

	numArgs := 1
	if len(args) != numArgs {
		return shim.Error("Invalid num of args. Expected " + strconv.Itoa(numArgs))
	}

	var id string
	id = args[0]

	queryResults, err := GetHistoryById(stub, id)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResults)
}

func (commentCcPtr *CommentChaincode) getAllComments(stub shim.ChaincodeStubInterface) pb.Response {
	fmt.Println("Executing getAllComments()")
	queryString := fmt.Sprintf(`
		{
			"selector": {"docType": "%s", "createDate": {"$gt": 0}},
			"sort": [{"createDate": "desc"}],
			"use_index": ["ddocCommentsAll", "indexCommentsAll"]
		}`, CommentDocType)

	queryResults, err := GetQueryResultFromQueryString(stub, queryString)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResults)
}

func (commentCcPtr *CommentChaincode) deleteComment(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("Executing deleteComment() args=" + strings.Join(args, ", "))

	numArgs := 2
	if len(args) != numArgs {
		return shim.Error("Invalid num of args. Expected " + strconv.Itoa(numArgs))
	}

	var senderId uint64
	var id string

	senderId, _ = strconv.ParseUint(args[0], Base, BitSize)
	id = args[1]

	key := CommentKey + id

	commentAsBytes, err := stub.GetState(key)
	if err != nil {
		return shim.Error("Error occurred while getting comment before update")
	} else if commentAsBytes == nil {
		return shim.Error("CommmentId does not exist")
	}

	updatedComment := Comment{}
	err = json.Unmarshal(commentAsBytes, &updatedComment)
	if err != nil {
		return shim.Error(err.Error())
	}

	if senderId != updatedComment.SenderId {
		return shim.Error("senderId passed is unauthorised to delete comment")
	}

	updatedComment.Deleted = true
	updatedComment.DeleteDate = GetCurrentTimestamp()

	updatedCommentAsBytes, err := json.Marshal(updatedComment)
	if err != nil {
		return shim.Error(err.Error())
	}
	err = stub.PutState(key, updatedCommentAsBytes)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = stub.DelState(key)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

func (commentCcPtr *CommentChaincode) getCommentsByRecipient(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("Executing getCommentsByRecipient() args=" + strings.Join(args, ", "))

	numArgs := 2
	if len(args) != numArgs {
		return shim.Error("Invalid num of args. Expected " + strconv.Itoa(numArgs))
	}

	var recipientType string
	var recipientId uint64

	recipientType = args[0]
	recipientId, _ = strconv.ParseUint(args[1], Base, BitSize)

	queryString := fmt.Sprintf(`
		{
			"selector": {"docType": "%s", "recipientType": "%s", "recipientId": %d, "createDate": {"$gte": 0}},
			"sort": [{"createDate": "desc"}],
			"use_index": ["ddocRecipient", "indexRecipient"]
		}`, CommentDocType, recipientType, recipientId)

	queryResults, err := GetQueryResultFromQueryString(stub, queryString)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResults)
}

func (commentCcPtr *CommentChaincode) getCommentsBySender(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("Executing getCommentsBySender() args=" + strings.Join(args, ", "))

	numArgs := 2
	if len(args) != numArgs {
		return shim.Error("Invalid num of args. Expected " + strconv.Itoa(numArgs))
	}

	var senderType string
	var senderId uint64

	senderType = args[0]
	senderId, _ = strconv.ParseUint(args[1], Base, BitSize)

	queryString := fmt.Sprintf(`
		{
			"selector": {"docType": "%s", "senderType": "%s", "senderId": %d, "createDate": {"$gte": 0}},
			"sort": [{"createDate": "desc"}],
			"use_index": ["ddocSender", "indexSender"]
		}`, CommentDocType, senderType, senderId)

	queryResults, err := GetQueryResultFromQueryString(stub, queryString)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResults)
}

func (commentCcPtr *CommentChaincode) getCommentsByExam(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("Executing getCommentsByExam() args=" + strings.Join(args, ", "))

	numArgs := 1
	if len(args) != numArgs {
		return shim.Error("Invalid num of args. Expected " + strconv.Itoa(numArgs))
	}

	var examId string
	examId = args[0]

	queryString := fmt.Sprintf(`
		{
			"selector": {"docType": "%s", "examId": "%s", "createDate": {"$gte": 0}},
			"sort": [{"createDate": "desc"}],
			"use_index": ["ddocExamId", "indexExamId"]
		}`, CommentDocType, examId)

	queryResults, err := GetQueryResultFromQueryString(stub, queryString)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResults)
}

func (commentCcPtr *CommentChaincode) getCommentsByItem(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("Executing getCommentsByItem() args=" + strings.Join(args, ", "))

	numArgs := 2
	if len(args) != numArgs {
		return shim.Error("Invalid num of args. Expected " + strconv.Itoa(numArgs))
	}

	var itemType string
	var itemId string

	itemType = args[0]
	itemId = args[1]

	queryString := fmt.Sprintf(`
		{
			"selector": {"docType": "%s", "itemType": "%s", "itemId": "%s", "createDate": {"$gte": 0}},
			"sort": [{"createDate": "desc"}],
			"use_index": ["ddocItem", "indexItem"]
		}`, CommentDocType, itemType, itemId)

	queryResults, err := GetQueryResultFromQueryString(stub, queryString)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResults)
}

func (commentCcPtr *CommentChaincode) createComment(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("Executing createComment() args=" + strings.Join(args, ", "))

	numArgs := 9
	if len(args) != numArgs {
		return shim.Error("Invalid num of args. Expected " + strconv.Itoa(numArgs))
	}

	var id string
	var text string
	var senderType string
	var senderId uint64
	var recipientType string
	var recipientId uint64
	var itemType string
	var itemId string
	var examId string

	id = args[0]
	text = args[1]
	senderType = args[2]
	senderId, _ = strconv.ParseUint(args[3], Base, BitSize)
	recipientType = args[4]
	recipientId, _ = strconv.ParseUint(args[5], Base, BitSize)
	itemType = args[6]
	itemId = args[7]
	examId = args[8]

	comment := Comment{
		Id:            id,
		Text:          text,
		SenderType:    senderType,
		SenderId:      senderId,
		RecipientType: recipientType,
		RecipientId:   recipientId,
		ItemType:      itemType,
		ItemId:        itemId,
		ExamId:        examId,
		CreateDate:    GetCurrentTimestamp(),
		LastUpdate:    GetCurrentTimestamp(),
		Deleted:       false,
		DeleteDate:    0,
		DocType:       CommentDocType}

	commentAsBytes, err := json.Marshal(comment)
	if err != nil {
		return shim.Error(err.Error())
	}

	key := CommentKey + id
	err = stub.PutState(key, commentAsBytes)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}
