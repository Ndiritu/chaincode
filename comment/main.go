package main

import (
	"fmt"
	"github.com/hyperledger/fabric/core/chaincode/shim"
)

func main() {
	err := shim.Start(new(CommentChaincode))
	if err != nil {
		fmt.Printf("Error starting Comment chaincode: %s", err)
	}
}
