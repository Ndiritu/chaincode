package main

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

type QuestionChaincode struct{}

type Question struct {
	Id           string `json:"id"`
	ExamId       string `json:"examId"`
	DisplayIndex int64  `json:"displayIndex"`
	Type         string `json:"type"`
	Text         string `json:"text"`
	Marks        int64  `json:"marks"`
	CreateDate   int64  `json:"createDate"`
	LastUpdate   int64  `json:"lastUpdate"`
	Deleted      bool   `json:"deleted"`
	DeleteDate   int64  `json:"deleteDate"`
	DocType      string `json:"docType"`
}

//TODO: updateQuestion() -> store log in questionHistory
//TODO; delete qstn & store record in history tbl

func (question Question) String() string {
	return fmt.Sprintf(`
		Question {
			id=%s,
			examId=%s,
			displayIndex=%d,
			type=%s,
			text=%s,
			marks=%d,
			createDate=%d,
			lastUpdate=%d,
			deleted=%v,
			deleteDate=%d,
			docType=%s
		}`,
		question.Id,
		question.ExamId,
		question.DisplayIndex,
		question.Type,
		question.Text,
		question.Marks,
		question.CreateDate,
		question.LastUpdate,
		question.Deleted,
		question.DeleteDate,
		question.DocType)
}

const (
	CreateQuestion       string = "createQuestion"
	UpdateQuestion       string = "updateQuestion"
	GetQuestionById      string = "getQuestionById"
	GetQuestionsByExamId string = "getQuestionsByExamId"
	GetAllQuestions      string = "getAllQuestions"
	GetQuestionHistory   string = "getQuestionHistory"
	DeleteQuestion       string = "deleteQuestion"
)

func (questionChaincodePtr *QuestionChaincode) Init(stub shim.ChaincodeStubInterface) pb.Response {
	return shim.Success(nil)
}

func (questionChaincodePtr *QuestionChaincode) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	function, args := stub.GetFunctionAndParameters()
	fmt.Println("\nInvoke() questionChaincode is running: " + function)

	switch function {
	case CreateQuestion:
		return questionChaincodePtr.createQuestion(stub, args)

	case UpdateQuestion:
		return questionChaincodePtr.updateQuestion(stub, args)

	case GetQuestionById:
		return questionChaincodePtr.getQuestionById(stub, args)

	case GetQuestionsByExamId:
		return questionChaincodePtr.getQuestionsByExamId(stub, args)

	case GetAllQuestions:
		return questionChaincodePtr.getAllQuestions(stub)

	case GetQuestionHistory:
		return questionChaincodePtr.getQuestionHistory(stub, args)

	case DeleteQuestion:
		return questionChaincodePtr.deleteQuestion(stub, args)

	default:
		fmt.Println("\nInvoke() did not find function: " + function)
		return shim.Error("Received unknown function invocation")
	}
}

func (questionCcPtr *QuestionChaincode) deleteQuestion(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("Executing deleteQuestion() args=" + strings.Join(args, ", "))

	numArgs := 2
	if len(args) != numArgs {
		return shim.Error("Invalid num of args. Expected " + strconv.Itoa(numArgs))
	}

	var lecturerId uint64
	var id string

	lecturerId, _ = strconv.ParseUint(args[0], Base, BitSize)
	id = args[1]

	key := QuestionKey + id

	qstnAsBytes, err := stub.GetState(key)
	if err != nil {
		return shim.Error("Error occurred while getting question before update")
	} else if qstnAsBytes == nil {
		return shim.Error("QuestionId does not exist")
	}

	updatedQuestion := Question{}
	err = json.Unmarshal(qstnAsBytes, &updatedQuestion)
	if err != nil {
		return shim.Error(err.Error())
	}

	//TODO: Query exam chaincode to verify lecId is creator of exam
	fmt.Println("Verify lecId: " + string(lecturerId) + " is creator of exam")

	updatedQuestion.Deleted = true
	updatedQuestion.DeleteDate = GetCurrentTimestamp()

	updatedQstnAsBytes, err := json.Marshal(updatedQuestion)
	if err != nil {
		return shim.Error(err.Error())
	}
	err = stub.PutState(key, updatedQstnAsBytes)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = stub.DelState(key)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

func (questionCcPtr *QuestionChaincode) getQuestionHistory(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("Executing getQuestionHistory() args=" + strings.Join(args, ", "))

	numArgs := 1
	if len(args) != numArgs {
		return shim.Error("Invalid num of args. Expected " + strconv.Itoa(numArgs))
	}

	var id string
	id = args[0]

	queryResult, err := GetHistoryById(stub, id)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResult)
}

func (questionCcPtr *QuestionChaincode) getAllQuestions(stub shim.ChaincodeStubInterface) pb.Response {
	fmt.Println("Executing getAllQuestions()")

	queryString := fmt.Sprintf(`
		{
			"selector": {"docType": "%s", "createDate": {"$gt": 0}},
			"sort": [{"createDate": "desc"}],
			"use_index": ["ddocQuestion", "indexQuestion"]}`, QuestionDocType)

	queryResult, err := GetQueryResultFromQueryString(stub, queryString)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResult)
}

func (questionChaincodePtr *QuestionChaincode) getQuestionsByExamId(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Print("Executing getQuestionsByExamId: args=" + strings.Join(args, ", ") + "\n")

	numArgs := 1
	if len(args) != numArgs {
		return shim.Error("Invalid number of arguments. Expected: " + strconv.Itoa(numArgs))
	}

	var examId string
	examId = args[0]

	queryString := fmt.Sprintf(`
		{
			"selector": {"docType": "%s", "examId": "%s", "displayIndex": {"$gte": 0}},
			"sort": [{"displayIndex": "desc"}],
			"use_index": ["ddocExamId", "indexExamId"]

		}`, QuestionDocType, examId)

	queryResults, err := GetQueryResultFromQueryString(stub, queryString)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResults)
}

func (questionChaincodePtr *QuestionChaincode) getQuestionById(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Print("Executing getQuestionById(): args=" + strings.Join(args, ", ") + "\n")

	numArgs := 1
	if len(args) != numArgs {
		return shim.Error("Invalid number of arguments. Expected: " + strconv.Itoa(numArgs))
	}

	var questionId string
	questionId = args[0]

	queryString := fmt.Sprintf(`
		{
			"selector": {"docType": "%s", "id": "%s"},
			"use_index": ["ddocQstnId", "indexQstnId"]
		}`, QuestionDocType, questionId)

	queryResults, err := GetQueryResultFromQueryString(stub, queryString)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResults)
}

func (questionChaincodePtr *QuestionChaincode) updateQuestion(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	numArgs := 6
	if len(args) != numArgs {
		return shim.Error("Invalid number of arguments. Expected " + strconv.Itoa(numArgs))
	}
	fmt.Print("Executing updateQuestion() : args=")
	fmt.Print(args)
	fmt.Print("\n")

	var questionId string
	var lecturerId uint64
	var displayIndex int64
	var qstnType string
	var qstnText string
	var marks int64

	questionId = args[0]
	lecturerId, _ = strconv.ParseUint(args[1], Base, BitSize)
	displayIndex, _ = strconv.ParseInt(args[2], Base, BitSize)
	qstnType = args[3]
	qstnText = args[4]
	marks, _ = strconv.ParseInt(args[5], Base, BitSize)

	// check that question exists
	key := QuestionKey + questionId
	qstnAsBytes, err := stub.GetState(key)

	if err != nil {
		return shim.Error("Error while retrieving question")
	}
	if qstnAsBytes == nil {
		return shim.Error("Question does not exist")
	}

	// check that lecturer_id is exam creator
	//TODO: call examination chaincode to verify lec_id
	fmt.Println("LecturerId: " + string(lecturerId))

	// var queryArgs []string
	// queryArgs[0] = "getExaminationById"
	// queryArgs[1] = strconv.FormatUint(lecturerId, Base)

	// queryResponse := stub.InvokeChaincode("ccExam", queryArgs, "channel-exams")
	// fmt.Print("QueryResponse: " + queryResponse.String())

	updatedQstn := Question{}
	err = json.Unmarshal(qstnAsBytes, &updatedQstn)
	if err != nil {
		return shim.Error(err.Error())
	}

	updatedQstn.DisplayIndex = displayIndex
	updatedQstn.Type = qstnType
	updatedQstn.Text = qstnText
	updatedQstn.Marks = marks
	updatedQstn.LastUpdate = GetCurrentTimestamp()

	updatedQstnAsBytes, err := json.Marshal(updatedQstn)
	if err != nil {
		return shim.Error(err.Error())
	}
	err = stub.PutState(key, updatedQstnAsBytes)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)

}

func (questionChaincodePtr *QuestionChaincode) createQuestion(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("\nExecuting createQuestion...")
	fmt.Println("Received args: " + strings.Join(args, ", "))

	numArgs := 6
	if len(args) != numArgs {
		return shim.Error("Invalid number of arguments. Expected: " + strconv.Itoa(numArgs))
	}

	var id string
	var examId string
	var displayIndex int64
	var qstnType string
	var text string
	var marks int64

	id = args[0]
	examId = args[1]
	displayIndex, _ = strconv.ParseInt(args[2], Base, BitSize)
	qstnType = args[3]
	text = args[4]
	marks, _ = strconv.ParseInt(args[5], Base, BitSize)

	var question = Question{
		Id:           id,
		ExamId:       examId,
		DisplayIndex: displayIndex,
		Type:         qstnType,
		Text:         text,
		Marks:        marks,
		CreateDate:   GetCurrentTimestamp(),
		LastUpdate:   GetCurrentTimestamp(),
		Deleted:      false,
		DeleteDate:   0,
		DocType:      QuestionDocType}

	qstnAsBytes, err := json.Marshal(question)
	if err != nil {
		return shim.Error(err.Error())
	}

	key := QuestionKey + question.Id
	err = stub.PutState(key, qstnAsBytes)

	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

func getQueryArgsForHistory(qstnHistoryId string, fnName string, question Question) [][]byte {
	fmt.Println("Executing getQueryArgsForHistory(): qstnHistId=" + qstnHistoryId + ", fnName=" + fnName + ", qstn=" + question.String())

	var queryArgs [][]byte
	var i int
	i = 0

	fnNameAsBytes := []byte(fnName)
	for j := 0; j <= len(fnNameAsBytes)-1; j++ {
		queryArgs[i][j] = fnNameAsBytes[j]
	}
	i++

	qstnIdAsBytes := []byte(question.Id)
	for j := 0; j <= len(qstnIdAsBytes)-1; j++ {
		queryArgs[i][j] = qstnIdAsBytes[j]
	}
	i++

	examIdAsBytes := []byte(question.ExamId)
	for j := 0; j <= len(examIdAsBytes)-1; j++ {
		queryArgs[0][j] = examIdAsBytes[j]
	}
	i++

	displayIndex := strconv.FormatInt(question.DisplayIndex, Base)
	displayIndexAsBytes := []byte(displayIndex)
	for j := 0; j <= len(displayIndexAsBytes)-1; j++ {
		queryArgs[0][j] = displayIndexAsBytes[j]
	}
	i++

	typeAsBytes := []byte(question.Type)
	for j := 0; j <= len(typeAsBytes)-1; j++ {
		queryArgs[0][j] = typeAsBytes[j]
	}
	i++

	textAsBytes := []byte(question.Text)
	for j := 0; j <= len(textAsBytes)-1; j++ {
		queryArgs[0][j] = textAsBytes[j]
	}
	i++

	marks := strconv.FormatInt(question.Marks, Base)
	marksAsBytes := []byte(marks)
	for j := 0; j <= len(marksAsBytes)-1; j++ {
		queryArgs[0][j] = marksAsBytes[j]
	}
	return queryArgs
}
