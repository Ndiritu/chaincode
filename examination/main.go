package main

import (
	"fmt"
	"github.com/hyperledger/fabric/core/chaincode/shim"
)

func main() {
	err := shim.Start(new(ExaminationChaincode))
	if err != nil {
		fmt.Printf("Error starting Examination Chaincode: %s", err)
	}
}
