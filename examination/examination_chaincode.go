package main

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

type ExaminationChaincode struct{}

type Examination struct {
	Id                       string `json:"id"`
	Name                     string `json:"name"`
	ExamType                 string `json:"examType"`
	CourseId                 uint64 `json:"courseId"`
	CreatedByLecturerId      uint64 `json:"createdByLecturerId"`
	UniversityId             uint64 `json:"universityId"`
	ExamDate                 int64  `json:"examDate"`
	StartTime                int64  `json:"startTime"`
	Duration                 int64  `json:"duration"`
	PrimaryNumberingFormat   string `json:"primaryNumberingFormat"`
	SecondaryNumberingFormat string `json:"secondaryNumberingFormat"`
	CreateDate               int64  `json:"createDate"`
	LastUpdate               int64  `json:"lastUpdate"`
	Deleted                  bool   `json:"deleted"`
	DeleteDate               int64  `json:"deleteDate"`
	DocType                  string `json:"docType"`
}

func (exam Examination) String() string {
	return fmt.Sprintf(`
		Examination{
			id=%s,
			name=%s,
			examType=%s,
			courseId=%d,
			createdByLecturerId=%d,
			universityId=%d,
			examDate=%d,
			startTime=%d,
			duration=%d,
			primaryNumberingFormat=%d,
			secondaryNumberingFormat=%d,
			createDate=%d,
			lastUpdate=%d,
			deleted=%v,
			deleteDate=%d,
			docType=%s 
		}
		`, exam.Id,
		exam.Name,
		exam.ExamType,
		exam.CourseId,
		exam.CreatedByLecturerId,
		exam.UniversityId,
		exam.ExamDate,
		exam.StartTime,
		exam.Duration,
		exam.PrimaryNumberingFormat,
		exam.SecondaryNumberingFormat,
		exam.CreateDate,
		exam.LastUpdate,
		exam.Deleted,
		exam.DeleteDate,
		exam.DocType)
}

const (
	CreateExamination           string = "createExamination"
	GetAllExaminations          string = "getAllExaminations"
	GetExaminationsByCourseId   string = "getExaminationsByCourseId"
	GetExaminationsByLecturerId string = "getExaminationsByLecturerId"
	GetExaminationById          string = "getExaminationById"
	UpdateExamination           string = "updateExamination"
	GetExamHistoryById          string = "getExamHistoryById"
	DeleteExamination           string = "deleteExamination"
)

func (examChaincodePtr *ExaminationChaincode) Init(stub shim.ChaincodeStubInterface) pb.Response {
	return shim.Success(nil)
}

func (examChaincodePtr *ExaminationChaincode) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	function, args := stub.GetFunctionAndParameters()
	fmt.Println("\nInvoke() examChaincode is running: " + function)

	switch function {
	case CreateExamination:
		return examChaincodePtr.createExamination(stub, args)

	case GetAllExaminations:
		return examChaincodePtr.getAllExaminations(stub)

	case GetExaminationsByCourseId:
		return examChaincodePtr.getExaminationsByCourseId(stub, args)

	case GetExaminationsByLecturerId:
		return examChaincodePtr.getExaminationsByLecturerId(stub, args)

	case GetExaminationById:
		return examChaincodePtr.getExaminationById(stub, args)

	case UpdateExamination:
		return examChaincodePtr.updateExamination(stub, args)

	case GetExamHistoryById:
		return examChaincodePtr.getExamHistoryById(stub, args)

	case DeleteExamination:
		return examChaincodePtr.deleteExamination(stub, args)

	default:
		fmt.Println("\nInvoke() did not find function: " + function)
		return shim.Error("Received unknown function invocation")
	}
}

func (examChaincodePtr *ExaminationChaincode) deleteExamination(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("Executing deleteExamination() args=" + strings.Join(args, ", "))

	numArgs := 2
	if len(args) != numArgs {
		return shim.Error("Invalid num of args. Expected " + strconv.Itoa(numArgs))
	}

	var id string
	var lecturerId uint64
	id = args[0]
	lecturerId, _ = strconv.ParseUint(args[1], Base, BitSize)

	key := ExamKey + id

	examAsBytes, err := stub.GetState(key)
	if err != nil {
		return shim.Error("Error occurred while getting exam before update")
	} else if examAsBytes == nil {
		return shim.Error("ExamId does not exist")
	}

	updatedExam := Examination{}
	err = json.Unmarshal(examAsBytes, &updatedExam)
	if err != nil {
		return shim.Error(err.Error())
	}

	if lecturerId != updatedExam.CreatedByLecturerId {
		return shim.Error("lecturerId is unauthorised to delete exam")
	}

	updatedExam.Deleted = true
	updatedExam.DeleteDate = GetCurrentTimestamp()

	updatedExamAsBytes, err := json.Marshal(updatedExam)
	if err != nil {
		return shim.Error(err.Error())
	}
	err = stub.PutState(key, updatedExamAsBytes)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = stub.DelState(key)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

func (examChaincodePtr *ExaminationChaincode) getExamHistoryById(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("Executing getExamHistoryById() args=" + strings.Join(args, ", "))

	numArgs := 1
	if len(args) != numArgs {
		return shim.Error("Invalid num of args. Expected " + strconv.Itoa(numArgs))
	}

	var id string
	id = args[0]

	queryResults, err := GetHistoryById(stub, id)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResults)
}

func (examChaincodePtr *ExaminationChaincode) updateExamination(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	if len(args) != 10 {
		return shim.Error("Invalid number of args")
	}

	fmt.Print("Executing updateExamination(): args=")
	fmt.Print(args)
	fmt.Print("\n")

	var examId string
	var lecturerId uint64
	var name string
	var examType string
	var courseId uint64
	var examDate int64
	var startTime int64
	var duration int64
	var primNumFormat string
	var secNumFormat string

	examId = args[0]
	lecturerId, _ = strconv.ParseUint(args[1], Base, BitSize)
	name = args[2]
	examType = args[3]
	courseId, _ = strconv.ParseUint(args[4], Base, BitSize)
	examDate, _ = strconv.ParseInt(args[5], Base, BitSize)
	startTime, _ = strconv.ParseInt(args[6], Base, BitSize)
	duration, _ = strconv.ParseInt(args[7], Base, BitSize)
	primNumFormat = args[8]
	secNumFormat = args[9]

	key := ExamKey + examId
	examAsBytes, err := stub.GetState(key)
	if err != nil {
		return shim.Error("Error occurred while getting exam before update")
	} else if examAsBytes == nil {
		return shim.Error("ExamId does not exist")
	}

	updatedExam := Examination{}
	err = json.Unmarshal(examAsBytes, &updatedExam)
	if err != nil {
		return shim.Error(err.Error())
	}

	if lecturerId != updatedExam.CreatedByLecturerId {
		return shim.Error("lecturerId is unauthorised to update exam")
	}

	updatedExam.Name = name
	updatedExam.ExamType = examType
	updatedExam.CourseId = courseId
	updatedExam.ExamDate = examDate
	updatedExam.StartTime = startTime
	updatedExam.Duration = duration
	updatedExam.PrimaryNumberingFormat = primNumFormat
	updatedExam.SecondaryNumberingFormat = secNumFormat
	updatedExam.LastUpdate = GetCurrentTimestamp()

	updatedExamAsBytes, err := json.Marshal(updatedExam)
	if err != nil {
		return shim.Error(err.Error())
	}
	err = stub.PutState(key, updatedExamAsBytes)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

func (examChaincodePtr *ExaminationChaincode) getExaminationById(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	if len(args) != 1 {
		return shim.Error("Invalid number of arguments")
	}
	fmt.Print("Executing getExaminationById(): args=")
	fmt.Print(args)
	fmt.Print("\n")

	var examId string
	examId = args[0]

	queryString := fmt.Sprintf(`
		{
			"selector": {"docType": "%s", "id": "%s"},
			"use_index": ["ddocExamId", "indexExamId"]
		}`, ExamDocType, examId)

	queryResults, err := GetQueryResultFromQueryString(stub, queryString)

	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResults)
}

func (examChaincodePtr *ExaminationChaincode) getExaminationsByLecturerId(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("Executing getExaminationByLecturerId : lecturerId=" + args[0])
	if len(args) != 1 {
		return shim.Error("Invalid number of arguments passed")
	}

	var lecturerId uint64
	lecturerId, _ = strconv.ParseUint(args[0], Base, BitSize)

	queryString := fmt.Sprintf(`
		{
			"selector": {"docType": "%s", "createdByLecturerId": %d, "lastUpdate": {"$gt": 0}},
			"sort": [{"lastUpdate": "desc"}],
			"use_index": ["ddocLecturerId", "indexLecturerId"]
		}`, ExamDocType, lecturerId)

	queryResults, err := GetQueryResultFromQueryString(stub, queryString)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResults)
}

func (examChaincodePtr *ExaminationChaincode) getExaminationsByCourseId(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("Executing getExaminationByCourseId() : courseId=" + args[0])
	if len(args) != 1 {
		return shim.Error("Invalid number of arguments passed")
	}

	var courseId uint64
	courseId, _ = strconv.ParseUint(args[0], Base, BitSize)

	queryString := fmt.Sprintf(`
		{
			"selector": {"docType": "%s", "courseId": %d, "lastUpdate": {"$gt": 0}},
			"sort": [{"lastUpdate": "desc"}],
			"use_index": ["ddocCourseId", "indexCourseId"]
		}`, ExamDocType, courseId)

	queryResults, err := GetQueryResultFromQueryString(stub, queryString)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(queryResults)
}

func (examChaincodePtr *ExaminationChaincode) createExamination(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("\nExecuting createExamination...")
	fmt.Println("Received args: " + strings.Join(args, ", "))

	var numArgs int = 11

	if len(args) != numArgs {
		return shim.Error("Incorrect number of arguments. Expecting " + string(numArgs))
	}

	var id string
	var name string
	var examType string
	var courseId uint64
	var createdByLecturerId uint64
	var universityId uint64
	var examDate int64
	var startTime int64
	var duration int64
	var primaryNumFormat string
	var secondaryNumFormat string

	id = args[0]
	name = args[1]
	examType = args[2]
	courseId, _ = strconv.ParseUint(args[3], Base, BitSize)
	createdByLecturerId, _ = strconv.ParseUint(args[4], Base, BitSize)
	universityId, _ = strconv.ParseUint(args[5], Base, BitSize)
	examDate, _ = strconv.ParseInt(args[6], Base, BitSize)
	startTime, _ = strconv.ParseInt(args[7], Base, BitSize)
	duration, _ = strconv.ParseInt(args[8], Base, BitSize)
	primaryNumFormat = args[9]
	secondaryNumFormat = args[10]

	var exam = Examination{
		Id:                       id,
		Name:                     name,
		ExamType:                 examType,
		CourseId:                 courseId,
		CreatedByLecturerId:      createdByLecturerId,
		UniversityId:             universityId,
		ExamDate:                 examDate,
		StartTime:                startTime,
		Duration:                 duration,
		PrimaryNumberingFormat:   primaryNumFormat,
		SecondaryNumberingFormat: secondaryNumFormat,
		CreateDate:               GetCurrentTimestamp(),
		LastUpdate:               GetCurrentTimestamp(),
		Deleted:                  false,
		DeleteDate:               0,
		DocType:                  ExamDocType}

	examAsBytes, marshallingErr := json.Marshal(exam)
	if marshallingErr != nil {
		fmt.Println("Marshalling error")
		return shim.Error(marshallingErr.Error())
	}

	key := ExamKey + exam.Id
	err := stub.PutState(key, examAsBytes)

	if err != nil {
		fmt.Println("Exiting createExamination after error")
		return shim.Error(err.Error())
	}

	fmt.Println("Exiting createExamination after success")
	return shim.Success(nil)
}

func (examChaincodePtr *ExaminationChaincode) getAllExaminations(stub shim.ChaincodeStubInterface) pb.Response {
	queryString := fmt.Sprintf(
		`{
			"selector": {"docType": "%s", "createDate": {"$gt": 0}},
			"sort": [{"createDate": "desc"}],
			"use_index": ["ddocExam", "indexExam"]
		}`, ExamDocType)

	queryResults, err := GetQueryResultFromQueryString(stub, queryString)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResults)
}
