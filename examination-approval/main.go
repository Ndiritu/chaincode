package main

import (
	"fmt"
	"github.com/hyperledger/fabric/core/chaincode/shim"
)

func main() {
	err := shim.Start(new(ExamApprovalChaincode))
	if err != nil {
		fmt.Printf("Error starting Exam Approval Chaincode: %s", err)
	}
}
