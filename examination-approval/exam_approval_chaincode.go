package main

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

type ExamApprovalChaincode struct{}

type ExamApproval struct {
	ExamId        string `json:"examId"`
	ChairpersonId uint64 `json:"chairpersonId"`
	Status        bool   `json:"status"`
	CreateDate    int64  `json:"createDate"`
	LastUpdate    int64  `json:"lastUpdate"`
	DocType       string `json:"docType"`
}

const (
	CreateExamApproval         string = "createExamApproval"
	UpdateExamApproval         string = "updateExamApproval"
	GetExamsByApprovalStatus   string = "getExamsByApprovalStatus"
	GetApprovalByExamId        string = "getApprovalByExamId"
	GetApprovalHistoryByExamId string = "getApprovalHistoryByExamId"
	GetAllExamApprovals        string = "getAllExamApprovals"
)

func (examApprovalCcPtr *ExamApprovalChaincode) createExamApproval(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("Executing createExamApproval() args=" + strings.Join(args, ", "))

	numArgs := 3
	if len(args) != numArgs {
		return shim.Error("Invalid num of args. Expected " + strconv.Itoa(numArgs))
	}

	var examId string
	var chairpersonId uint64
	var status bool

	examId = args[0]
	chairpersonId, _ = strconv.ParseUint(args[1], Base, BitSize)
	status, _ = strconv.ParseBool(args[2])

	var examApproval = ExamApproval{
		ExamId:        examId,
		ChairpersonId: chairpersonId,
		Status:        status,
		CreateDate:    GetCurrentTimestamp(),
		LastUpdate:    GetCurrentTimestamp(),
		DocType:       ExamApprovalDocType}

	examApprovalAsBytes, err := json.Marshal(examApproval)
	if err != nil {
		return shim.Error(err.Error())
	}

	key := ExamApprovalKey + examId
	err = stub.PutState(key, examApprovalAsBytes)

	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)

}

func (examApprovalCcPtr *ExamApprovalChaincode) updateExamApproval(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("Executing updateExamApproval() args=" + strings.Join(args, ", "))

	numArgs := 3
	if len(args) != numArgs {
		return shim.Error("Invalid num of args. Expected " + strconv.Itoa(numArgs))
	}

	var examId string
	var chairpersonId uint64
	var status bool

	examId = args[0]
	chairpersonId, _ = strconv.ParseUint(args[1], Base, BitSize)
	status, _ = strconv.ParseBool(args[2])

	key := ExamApprovalKey + examId
	examApprovalAsBytes, err := stub.GetState(key)
	if err != nil {
		return shim.Error(err.Error())
	} else if examApprovalAsBytes == nil {
		return shim.Error("Exam approval doesn't exist")
	}

	updatedExamApproval := ExamApproval{}

	err = json.Unmarshal(examApprovalAsBytes, &updatedExamApproval)
	if err != nil {
		return shim.Error(err.Error())
	}

	if chairpersonId != updatedExamApproval.ChairpersonId {
		return shim.Error("chairpersonId is unauthorised to update approval")
	}

	updatedExamApproval.Status = status
	updatedExamApproval.LastUpdate = GetCurrentTimestamp()

	updatedExamApprovalAsBytes, err := json.Marshal(updatedExamApproval)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = stub.PutState(key, updatedExamApprovalAsBytes)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

func (examApprovalCcPtr *ExamApprovalChaincode) getExamsByApprovalStatus(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("Executing getExamsByApprovalStatus() args=" + strings.Join(args, ", "))

	numArgs := 2
	if len(args) != numArgs {
		return shim.Error("Invalid num of args. Expected " + strconv.Itoa(numArgs))
	}

	var chairpersonId uint64
	var status bool

	chairpersonId, _ = strconv.ParseUint(args[0], Base, BitSize)
	status, _ = strconv.ParseBool(args[1])

	queryString := fmt.Sprintf(`
		{
			"selector": {"docType": "%s", "chairpersonId": %d, "status": %v, "createDate": {"$gt": 0}},
			"sort": [{"createDate": "desc"}],
			"use_index": ["ddocExamApprovalStatus", "indexExamApprovalStatus"]
		}`, ExamApprovalDocType, chairpersonId, status)

	queryResult, err := GetQueryResultFromQueryString(stub, queryString)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResult)
}

func (examApprovalCcPtr *ExamApprovalChaincode) getApprovalByExamId(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("Executing getApprovalByExamId() args=" + strings.Join(args, ", "))

	numArgs := 1
	if len(args) != numArgs {
		return shim.Error("Invalid num of args. Expected " + strconv.Itoa(numArgs))
	}

	var examId string
	examId = args[0]

	queryString := fmt.Sprintf(`
		{
			"selector": {"docType": "%s", "examId": "%s"},
			"use_index": ["ddocExamId", "indexExamId"]
		}`, ExamApprovalDocType, examId)

	queryResults, err := GetQueryResultFromQueryString(stub, queryString)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResults)
}

func (examApprovalCcPtr *ExamApprovalChaincode) getApprovalHistoryByExamId(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("Executing getApprovalHistoryByExamId() args=" + strings.Join(args, ", "))

	numArgs := 1
	if len(args) != numArgs {
		return shim.Error("Invalid num of args. Expected " + strconv.Itoa(numArgs))
	}

	var examId string
	examId = args[0]

	queryResult, err := GetHistoryById(stub, examId)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResult)
}

func (examApprovalCcPtr *ExamApprovalChaincode) getAllExamApprovals(stub shim.ChaincodeStubInterface) pb.Response {
	fmt.Println("Executing getAllExamApprovals()")

	queryString := fmt.Sprintf(`
		{
			"selector": {"docType": "%s", "createDate": {"$gt": 0}},
			"sort": [{"createDate": "desc"}],
			"use_index": ["ddocApproval", "indexApproval"]

		}`, ExamApprovalDocType)

	queryResult, err := GetQueryResultFromQueryString(stub, queryString)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResult)
}

func (examApprovalCcPtr *ExamApprovalChaincode) Init(stub shim.ChaincodeStubInterface) pb.Response {
	return shim.Success(nil)
}

func (examApprovalCcPtr *ExamApprovalChaincode) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	fn, args := stub.GetFunctionAndParameters()
	fmt.Println("Invoke() is running: " + fn)

	switch fn {
	case CreateExamApproval:
		return examApprovalCcPtr.createExamApproval(stub, args)

	case UpdateExamApproval:
		return examApprovalCcPtr.updateExamApproval(stub, args)

	case GetApprovalByExamId:
		return examApprovalCcPtr.getApprovalByExamId(stub, args)

	case GetApprovalHistoryByExamId:
		return examApprovalCcPtr.getApprovalHistoryByExamId(stub, args)

	case GetExamsByApprovalStatus:
		return examApprovalCcPtr.getExamsByApprovalStatus(stub, args)

	case GetAllExamApprovals:
		return examApprovalCcPtr.getAllExamApprovals(stub)

	default:
		fmt.Println("Invoke() did not find fn: " + fn)
		return shim.Error("Could not find fn: " + fn)
	}
}
