package main

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

type SectionChaincode struct{}

type Section struct {
	Id           string `json:"id"`
	ExamId       string `json:"examId"`
	DisplayIndex int64  `json:"displayIndex"`
	Name         string `json:"name"`
	CreateDate   int64  `json:"createDate"`
	LastUpdate   int64  `json:"lastUpdate"`
	Deleted      bool   `json:"deleted"`
	DeleteDate   int64  `json:"deleteDate"`
	DocType      string `json:"docType"`
}

func (section Section) String() string {
	return fmt.Sprintf(`
		Section {
			id=%s,
			examId=%s,
			displayIndex=%d,
			name=%s,
			createDate=%d,
			lastUpdate=%d,
			deleted=%v,
			deleteDate=%d,
			docType=%s
		}`,
		section.Id,
		section.ExamId,
		section.DisplayIndex,
		section.Name,
		section.CreateDate,
		section.LastUpdate,
		section.Deleted,
		section.DeleteDate,
		section.DocType)
}

const (
	CreateSection       string = "createSection"
	UpdateSection       string = "updateSection"
	GetSectionsByExamId string = "getSectionsByExamId"
	GetSectionById      string = "getSectionById"
	GetAllSections      string = "getAllSections"
	DeleteSection       string = "deleteSection"
	GetSectionHistory   string = "getSectionHistory"
)

func (sectionCcPtr *SectionChaincode) Init(stub shim.ChaincodeStubInterface) pb.Response {
	return shim.Success(nil)
}

func (sectionCcPtr *SectionChaincode) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	fn, args := stub.GetFunctionAndParameters()
	fmt.Println("Invoke() received fn: " + fn)

	switch fn {
	case CreateSection:
		return sectionCcPtr.createSection(stub, args)

	case UpdateSection:
		return sectionCcPtr.updateSection(stub, args)

	case GetSectionsByExamId:
		return sectionCcPtr.getSectionsByExamId(stub, args)

	case GetSectionById:
		return sectionCcPtr.getSectionById(stub, args)

	case GetAllSections:
		return sectionCcPtr.getAllSections(stub)

	case DeleteSection:
		return sectionCcPtr.deleteSection(stub, args)

	case GetSectionHistory:
		return sectionCcPtr.getSectionHistory(stub, args)

	default:
		fmt.Println("Did not find fn: " + fn)
		return shim.Error("Unknown fn: " + fn)
	}
}

func (sectionCcPtr *SectionChaincode) getSectionHistory(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("Executing getSectionHistory() args=" + strings.Join(args, ", "))

	numArgs := 1
	if len(args) != numArgs {
		return shim.Error("Invalid num of args. Expected " + strconv.Itoa(numArgs))
	}

	var sectionId string
	sectionId = args[0]

	queryResult, err := GetHistoryById(stub, sectionId)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResult)
}

func (sectionCcPtr *SectionChaincode) deleteSection(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("Executing deleteSection() args=" + strings.Join(args, ", "))

	numArgs := 2
	if len(args) != numArgs {
		return shim.Error("Invalid num of args. Expected " + strconv.Itoa(numArgs))
	}

	var lecturerId uint64
	var sectionId string

	lecturerId, _ = strconv.ParseUint(args[0], Base, BitSize)
	sectionId = args[1]

	key := SectionKey + sectionId

	sectionAsBytes, err := stub.GetState(key)
	if err != nil {
		return shim.Error("Error occurred while getting section before update")
	} else if sectionAsBytes == nil {
		return shim.Error("sectionId does not exist")
	}

	updatedSection := Section{}
	err = json.Unmarshal(sectionAsBytes, &updatedSection)
	if err != nil {
		return shim.Error(err.Error())
	}

	//todo: verify lec id is creator
	fmt.Print("Verify lecId: " + string(lecturerId) + ", is creator")

	updatedSection.Deleted = true
	updatedSection.DeleteDate = GetCurrentTimestamp()

	fmt.Println("After update, updatedSection now contains: " + updatedSection.String())

	updatedSectionAsBytes, err := json.Marshal(updatedSection)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = stub.PutState(key, updatedSectionAsBytes)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = stub.DelState(key)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(nil)
}

func (sectionCcPtr *SectionChaincode) getAllSections(stub shim.ChaincodeStubInterface) pb.Response {
	fmt.Println("Executing getAllSections()")

	queryString := fmt.Sprintf(`
		{
			"selector": {"docType": "%s", "createDate": {"$gt": 0}},
			"sort": [{"createDate": "desc"}],
			"use_index": ["ddocSection", "indexSection"]
		}`, SectionDocType)

	queryResult, err := GetQueryResultFromQueryString(stub, queryString)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResult)
}

func (sectionCcPtr *SectionChaincode) getSectionById(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("Executing getSectionById() args=" + strings.Join(args, ", "))

	numArgs := 1
	if len(args) != numArgs {
		return shim.Error("Invalid num of args. Expected " + strconv.Itoa(numArgs))
	}

	var sectionId string
	sectionId = args[0]

	queryString := fmt.Sprintf(`
		{
			"selector": {"docType": "%s", "id": "%s"},
			"use_index": ["ddocSectionId", "indexSectionId"]
		}`, SectionDocType, sectionId)

	queryResults, err := GetQueryResultFromQueryString(stub, queryString)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResults)
}

func (sectionCcPtr *SectionChaincode) getSectionsByExamId(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("Executing getSectionsByExamId() args=" + strings.Join(args, ", "))

	numArgs := 1
	if len(args) != numArgs {
		return shim.Error("Invalid num of args. Expected " + strconv.Itoa(numArgs))
	}

	var examId string
	examId = args[0]

	queryString := fmt.Sprintf(`
		{
			"selector": {"docType": "%s", "examId": "%s", "displayIndex":{"$gte": 0}},
			"sort": [{"displayIndex": "desc"}],
			"use_index": ["ddocExamId", "indexExamId"]
		}`, SectionDocType, examId)

	queryResults, err := GetQueryResultFromQueryString(stub, queryString)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResults)
}

func (sectionCcPtr *SectionChaincode) updateSection(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("Executing updateSection() args=" + strings.Join(args, ", "))

	numArgs := 4
	if len(args) != numArgs {
		return shim.Error("Invalid num of args. Expected " + strconv.Itoa(numArgs))
	}

	var lecturerId uint64
	var sectionId string
	var displayIndex int64
	var name string

	lecturerId, _ = strconv.ParseUint(args[0], Base, BitSize)
	sectionId = args[1]
	displayIndex, _ = strconv.ParseInt(args[2], Base, BitSize)
	name = args[3]

	//check if sectionId exists
	key := SectionKey + sectionId
	sectionAsBytes, err := stub.GetState(key)
	if err != nil {
		return shim.Error("Error verifying section")
	}
	if sectionAsBytes == nil {
		return shim.Error("Section does not exist")
	}

	updatedSection := Section{}
	err = json.Unmarshal(sectionAsBytes, &updatedSection)
	if err != nil {
		return shim.Error(err.Error())
	}

	//todo: call exam ccd to verify lec id is creator of exam
	fmt.Println("Verify lec id: " + string(lecturerId) + " is exam creator")

	updatedSection.DisplayIndex = displayIndex
	updatedSection.Name = name
	updatedSection.LastUpdate = GetCurrentTimestamp()

	updatedSectionAsBytes, err := json.Marshal(updatedSection)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = stub.PutState(key, updatedSectionAsBytes)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

func (sectionCcPtr *SectionChaincode) createSection(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("Executing createSection() : args=" + strings.Join(args, ", "))

	numArgs := 4
	if len(args) != numArgs {
		return shim.Error("Invalid num of args. Expected: " + strconv.Itoa(numArgs))
	}

	var id string
	var examId string
	var displayIndex int64
	var name string

	id = args[0]
	examId = args[1]
	displayIndex, _ = strconv.ParseInt(args[2], Base, BitSize)
	name = args[3]

	section := Section{
		Id:           id,
		ExamId:       examId,
		DisplayIndex: displayIndex,
		Name:         name,
		CreateDate:   GetCurrentTimestamp(),
		LastUpdate:   GetCurrentTimestamp(),
		Deleted:      false,
		DeleteDate:   0,
		DocType:      SectionDocType}

	sectionAsBytes, err := json.Marshal(section)
	if err != nil {
		return shim.Error(err.Error())
	}

	key := SectionKey + id
	err = stub.PutState(key, sectionAsBytes)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}
