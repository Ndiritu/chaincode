package main

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

type InstructionChaincode struct{}

type Instruction struct {
	Id           string `json:"id"`
	ExamId       string `json:"examId"`
	DisplayIndex int64  `json:"displayIndex"`
	Type         string `json:"type"`
	Text         string `json:"text"`
	CreateDate   int64  `json:"createDate"`
	LastUpdate   int64  `json:"lastUpdate"`
	Deleted      bool   `json:"deleted"`
	DeleteDate   int64  `json:"deleteDate"`
	DocType      string `json:"docType"`
}

func (instruction Instruction) String() string {
	return fmt.Sprintf(`
		Instruction {
			id=%s,
			examId=%s,
			displayIndex=%d,
			type=%s,
			text=%s,
			createDate=%d,
			lastUpdate=%d,
			deleted=%v,
			deleteDate=%d,
			docType=%s
		}`,
		instruction.Id,
		instruction.ExamId,
		instruction.DisplayIndex,
		instruction.Type,
		instruction.Text,
		instruction.CreateDate,
		instruction.LastUpdate,
		instruction.Deleted,
		instruction.DeleteDate,
		instruction.DocType)
}

const (
	CreateInstruction         = "createInstruction"
	UpdateInstruction         = "updateInstruction"
	DeleteInstruction         = "deleteInstruction"
	GetInstructionsByExamId   = "getInstructionsByExamId"
	GetInstructionById        = "getInstructionById"
	GetInstructionHistoryById = "getInstructionHistoryById"
)

func (instructionCcPtr *InstructionChaincode) Init(stub shim.ChaincodeStubInterface) pb.Response {
	return shim.Success(nil)
}

func (instructionCcPtr *InstructionChaincode) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	fn, args := stub.GetFunctionAndParameters()
	fmt.Println("Invoke() is running : " + fn)

	switch fn {
	case CreateInstruction:
		return instructionCcPtr.createInstruction(stub, args)

	case UpdateInstruction:
		return instructionCcPtr.updateInstruction(stub, args)

	case GetInstructionsByExamId:
		return instructionCcPtr.getInstructionsByExamId(stub, args)

	case GetInstructionById:
		return instructionCcPtr.getInstructionById(stub, args)

	case GetInstructionHistoryById:
		return instructionCcPtr.getInstructionHistoryById(stub, args)

	case DeleteInstruction:
		return instructionCcPtr.deleteInstruction(stub, args)

	default:
		fmt.Println("Invoke() did not find function :" + fn)
		return shim.Error("Unknown function")
	}

}

func (instructionCcdPtr *InstructionChaincode) getAllInstructions(stub shim.ChaincodeStubInterface) pb.Response {
	fmt.Println("Executing getAllInstructions()")

	queryString := fmt.Sprintf(`
		{
			"selector": {"docType": "%s", "createDate": {"$gt": 0}},
			"sort": [{"createDate": "desc"}],
			"use_index": ["ddocInstruction", "indexInstruction"]
		}`, InstructionDocType)

	queryResults, err := GetQueryResultFromQueryString(stub, queryString)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResults)
}

func (instructionCcdPtr *InstructionChaincode) deleteInstruction(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("Executing deleteInstruction() args=" + strings.Join(args, ", "))

	numArgs := 2
	if len(args) != numArgs {
		return shim.Error("Invalid num of args. Expected=" + strconv.Itoa(numArgs))
	}

	var lecturerId uint64
	var id string

	lecturerId, _ = strconv.ParseUint(args[0], Base, BitSize)
	id = args[1]

	key := InstructionKey + id

	instructionAsBytes, err := stub.GetState(key)
	if err != nil {
		return shim.Error("Error occurred while getting instruction before update")
	} else if instructionAsBytes == nil {
		return shim.Error("CommmentId does not exist")
	}

	updatedInstruction := Instruction{}
	err = json.Unmarshal(instructionAsBytes, &updatedInstruction)
	if err != nil {
		return shim.Error(err.Error())
	}

	//TODO: Call exam chaincode to get lec_id from exam
	fmt.Println("Ensure lec id: " + string(lecturerId) + " is creator of instruction")

	updatedInstruction.Deleted = true
	updatedInstruction.DeleteDate = GetCurrentTimestamp()

	updatedInstructionAsBytes, err := json.Marshal(updatedInstruction)
	if err != nil {
		return shim.Error(err.Error())
	}
	err = stub.PutState(key, updatedInstructionAsBytes)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = stub.DelState(key)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

func (instructionCcdPtr *InstructionChaincode) getInstructionHistoryById(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("Executing getInstructionHistoryById() args=" + strings.Join(args, ", "))

	numArgs := 1
	if len(args) != numArgs {
		return shim.Error("Invalid num of args. Expected " + strconv.Itoa(numArgs))
	}

	var id string
	id = args[0]

	queryResult, err := GetHistoryById(stub, id)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResult)
}

func (instructionCcPtr *InstructionChaincode) getInstructionById(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("Executing getInstructionById() args=" + strings.Join(args, ", "))

	numArgs := 1
	if len(args) != numArgs {
		return shim.Error("Invalid num of args. Expected " + strconv.Itoa(numArgs))
	}

	var instructionId string
	instructionId = args[0]

	queryString := fmt.Sprintf(`
		{
			"selector": {"docType": "%s", "id": "%s"},
			"use_index": ["ddocInstructionId", "indexInstructionId"]
		}`, InstructionDocType, instructionId)

	queryResults, err := GetQueryResultFromQueryString(stub, queryString)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResults)
}

func (instructionCcPtr *InstructionChaincode) updateInstruction(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("Executing updateInstruction(): args=" + strings.Join(args, ", "))

	numArgs := 5
	if len(args) != numArgs {
		return shim.Error("Invalid num of args. Expected " + strconv.Itoa(numArgs))
	}

	var instructionId string
	var lecturerId uint64
	var displayIndex int64
	var instructionType string
	var text string

	instructionId = args[0]
	lecturerId, _ = strconv.ParseUint(args[1], Base, BitSize)
	displayIndex, _ = strconv.ParseInt(args[2], Base, BitSize)
	instructionType = args[3]
	text = args[4]

	//check that instruction exists
	key := InstructionKey + instructionId
	instructionAsBytes, err := stub.GetState(key)

	if err != nil {
		return shim.Error("Error while verifying instructionId")
	}
	if instructionAsBytes == nil {
		return shim.Error("Instruction does not exist")
	}

	//TODO; Verify that lecturerId is exam creator (Call exam chaincode)
	fmt.Println("Verify lecturerId: " + strconv.Itoa(int(lecturerId)))

	updatedInstruction := Instruction{}
	err = json.Unmarshal(instructionAsBytes, &updatedInstruction)
	if err != nil {
		return shim.Error(err.Error())
	}

	updatedInstruction.DisplayIndex = displayIndex
	updatedInstruction.Type = instructionType
	updatedInstruction.Text = text
	updatedInstruction.LastUpdate = GetCurrentTimestamp()

	updatedInstructAsBytes, err := json.Marshal(updatedInstruction)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = stub.PutState(key, updatedInstructAsBytes)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

func (instructionCcPtr *InstructionChaincode) getInstructionsByExamId(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("Executing getInstructionByExamId(): args=" + strings.Join(args, ", "))

	numArgs := 1
	if len(args) != numArgs {
		return shim.Error("Invalid num of args. Expected " + strconv.Itoa(numArgs))
	}

	var examId string
	examId = args[0]

	queryString := fmt.Sprintf(`
		{
			"selector": {"docType": "%s", "examId": "%s", "displayIndex":{"$gte": 0}},
			"sort": [{"displayIndex": "desc"}],
			"use_index": ["ddocExamId", "indexExamId"]
		}`, InstructionDocType, examId)

	queryResults, err := GetQueryResultFromQueryString(stub, queryString)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(queryResults)
}

func (instructionCcPtr *InstructionChaincode) createInstruction(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("Executing createInstruction(): args=" + strings.Join(args, ", "))

	numArgs := 5
	if len(args) != numArgs {
		return shim.Error("Invalid no. of args. Expected: " + strconv.Itoa(numArgs))
	}

	var id string
	var examId string
	var displayIndex int64
	var instructionType string
	var text string

	id = args[0]
	examId = args[1]
	displayIndex, _ = strconv.ParseInt(args[2], Base, BitSize)
	instructionType = args[3]
	text = args[4]

	instruction := Instruction{
		Id:           id,
		ExamId:       examId,
		DisplayIndex: displayIndex,
		Type:         instructionType,
		Text:         text,
		CreateDate:   GetCurrentTimestamp(),
		LastUpdate:   GetCurrentTimestamp(),
		Deleted:      false,
		DeleteDate:   0,
		DocType:      InstructionDocType}

	instructionAsBytes, err := json.Marshal(instruction)
	if err != nil {
		return shim.Error(err.Error())
	}

	key := InstructionKey + id
	err = stub.PutState(key, instructionAsBytes)

	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}
