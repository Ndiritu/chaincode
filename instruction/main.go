package main

import (
	"fmt"
	"github.com/hyperledger/fabric/core/chaincode/shim"
)

func main() {
	err := shim.Start(new(InstructionChaincode))
	if err != nil {
		fmt.Printf("Error starting Question chaincode: %s", err)
	}
}
