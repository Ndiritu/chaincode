package main

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

type ExamGenerationChaincode struct{}

type ExamGeneration struct {
	ExamId              string `json:"examId"`
	ExamCentreStaffId   uint64 `json:"examCentreStaffId"`
	ExamCentreStatus    bool   `json:"examCentreStatus"`
	ExamCentreNumCopies int64  `json:"examCentreNumCopies"`
	LecturerId          uint64 `json:"lecturerId"`
	LecturerStatus      bool   `json:"lecturerStatus"`
	LecturerNumCopies   int64  `json:"lecturerNumCopies"`
	CreateDate          int64  `json:"createDate"`
	LastUpdate          int64  `json:"lastUpdate"`
	DocType             string `json:"docType"`
}

const (
	CreateExamGeneration                 string = "createExamGeneration"
	UpdateExamGeneration                 string = "updateExamGeneration"
	GetGenerationInfoByExamId            string = "getGenerationInfoByExamId"
	GetGenerationInfoByExamCentreStaffId string = "getGenerationInfoByExamCentreStaffId"
	GetAllExamGenerations                string = "getAllExamGenerations"
)

func (examGenerationCcPtr *ExamGenerationChaincode) createExamGeneration(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("Executing createExamGeneration() args=" + strings.Join(args, ", "))

	numArgs := 7
	if len(args) != numArgs {
		return shim.Error("Invalid num of args. Expected " + strconv.Itoa(numArgs))
	}

	var examId string
	var examCentreStaffId uint64
	var examCentreStatus bool
	var examCentreNumCopies int64
	var lecturerId uint64
	var lecturerStatus bool
	var lecturerNumCopies int64

	examId = args[0]
	examCentreStaffId, _ = strconv.ParseUint(args[1], Base, BitSize)
	examCentreStatus, _ = strconv.ParseBool(args[2])
	examCentreNumCopies, _ = strconv.ParseInt(args[3], Base, BitSize)
	lecturerId, _ = strconv.ParseUint(args[4], Base, BitSize)
	lecturerStatus, _ = strconv.ParseBool(args[5])
	lecturerNumCopies, _ = strconv.ParseInt(args[6], Base, BitSize)

	var examGeneration = ExamGeneration{
		ExamId:              examId,
		ExamCentreStaffId:   examCentreStaffId,
		ExamCentreStatus:    examCentreStatus,
		ExamCentreNumCopies: examCentreNumCopies,
		LecturerId:          lecturerId,
		LecturerStatus:      lecturerStatus,
		LecturerNumCopies:   lecturerNumCopies,
		CreateDate:          GetCurrentTimestamp(),
		LastUpdate:          GetCurrentTimestamp(),
		DocType:             ExamGenerationDocType}

	examGenAsBytes, err := json.Marshal(examGeneration)
	if err != nil {
		return shim.Error(err.Error())
	}

	key := ExamGenerationKey + examId
	err = stub.PutState(key, examGenAsBytes)

	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

func (examGenerationCcPtr *ExamGenerationChaincode) updateExamGeneration(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("Executing updateExamGeneration() args=" + strings.Join(args, ", "))

	numArgs := 5
	if len(args) != numArgs {
		return shim.Error("Invalid num of args. Expected " + strconv.Itoa(numArgs))
	}

	var examId string
	var userType string
	var userId uint64
	var status bool
	var numCopies int64

	examId = args[0]
	userType = args[1]
	userId, _ = strconv.ParseUint(args[2], Base, BitSize)
	status, _ = strconv.ParseBool(args[3])
	numCopies, _ = strconv.ParseInt(args[4], Base, BitSize)

	key := ExamGenerationKey + examId
	examGenAsBytes, err := stub.GetState(key)
	if err != nil {
		return shim.Error(err.Error())
	} else if examGenAsBytes == nil {
		return shim.Error("Exam Generation doesn't exist")
	}

	updatedExamGeneration := ExamGeneration{}

	err = json.Unmarshal(examGenAsBytes, &updatedExamGeneration)
	if err != nil {
		return shim.Error(err.Error())
	}

	if userType == ExamCentreStaffUserType {
		if userId != updatedExamGeneration.ExamCentreStaffId {
			return shim.Error("Exam centre staff unathorised to update exam generation")
		}
		updatedExamGeneration.ExamCentreStatus = status
		updatedExamGeneration.ExamCentreNumCopies = numCopies
	}
	if userType == LecturerUserType {
		if userId != updatedExamGeneration.LecturerId {
			return shim.Error("Lecturer unauthorised to update exam generation")
		}
		updatedExamGeneration.LecturerStatus = status
		updatedExamGeneration.LecturerNumCopies = numCopies
	}
	updatedExamGeneration.LastUpdate = GetCurrentTimestamp()

	updatedExamGenAsBytes, err := json.Marshal(updatedExamGeneration)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = stub.PutState(key, updatedExamGenAsBytes)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(nil)
}

func (examGenerationCcPtr *ExamGenerationChaincode) getGenerationInfoByExamId(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("Executing getGenerationInfoByExamId() args=" + strings.Join(args, ", "))

	numArgs := 1
	if len(args) != numArgs {
		return shim.Error("Invalid num of args. Expected " + strconv.Itoa(numArgs))
	}

	var examId string
	examId = args[0]

	queryString := fmt.Sprintf(`
		{
			"selector": {"docType": "%s", "examId": "%s"},
			"use_index": ["ddocExamId", "indexExamId"]
		}`, ExamGenerationDocType, examId)

	queryResults, err := GetQueryResultFromQueryString(stub, queryString)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResults)
}

func (examGenerationCcPtr *ExamGenerationChaincode) getGenerationInfoByExamCentreStaffId(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("Executing getGenerationInfoByExamCentreStaffId() args=" + strings.Join(args, ", "))

	numArgs := 1
	if len(args) != numArgs {
		return shim.Error("Invalid num of args. Expected " + strconv.Itoa(numArgs))
	}

	var examCentreStaffId uint64
	examCentreStaffId, _ = strconv.ParseUint(args[0], Base, BitSize)

	queryString := fmt.Sprintf(`
		{
			"selector": {"docType": "%s", "examCentreStaffId": %d},
			"use_index": ["ddocExamCentreStaffId", "indexExamCentreStaffId"]
		}`, ExamGenerationDocType, examCentreStaffId)

	queryResults, err := GetQueryResultFromQueryString(stub, queryString)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResults)
}

func (examGenerationCcPtr *ExamGenerationChaincode) getAllExamGenerations(stub shim.ChaincodeStubInterface) pb.Response {
	fmt.Println("Executing getAllExamGenerations()")

	queryString := fmt.Sprintf(`
		{
			"selector": {"docType": "%s", "createDate": {"$gt": 0}},
			"sort": [{"createDate": "desc"}],
			"use_index": ["ddocGeneration", "indexGeneration"]
		}`, ExamGenerationDocType)

	queryResult, err := GetQueryResultFromQueryString(stub, queryString)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResult)
}

func (examGenerationCcPtr *ExamGenerationChaincode) Init(stub shim.ChaincodeStubInterface) pb.Response {
	return shim.Success(nil)
}

func (examGenerationCcPtr *ExamGenerationChaincode) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	fn, args := stub.GetFunctionAndParameters()
	fmt.Println("Invoke() is running: " + fn)

	switch fn {
	case CreateExamGeneration:
		return examGenerationCcPtr.createExamGeneration(stub, args)

	case UpdateExamGeneration:
		return examGenerationCcPtr.updateExamGeneration(stub, args)

	case GetGenerationInfoByExamId:
		return examGenerationCcPtr.getGenerationInfoByExamId(stub, args)

	case GetGenerationInfoByExamCentreStaffId:
		return examGenerationCcPtr.getGenerationInfoByExamCentreStaffId(stub, args)

	case GetAllExamGenerations:
		return examGenerationCcPtr.getAllExamGenerations(stub)

	default:
		fmt.Println("Invoke() did not find fn: " + fn)
		return shim.Error("Could not find fn: " + fn)

	}
}
