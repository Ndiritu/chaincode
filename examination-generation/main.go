package main

import (
	"fmt"
	"github.com/hyperledger/fabric/core/chaincode/shim"
)

func main() {
	err := shim.Start(new(ExamGenerationChaincode))
	if err != nil {
		fmt.Printf("Error starting Exam Generation Chaincode: %s", err)
	}
}
